#!/bin/sh

set -e
set -u
#set -x

export COMPOSE_PROJECT_NAME=system
export HOST_IP=$(ip addr show docker0 | grep -w inet | sed 's%.*inet \([^/]*\).*%\1%')
export DNS_IP=$(grep nameserver /etc/resolv.conf | head -n1 |cut -d' ' -f2)
export USER_ID=$(id -u)
export GROUP_ID=$(id -g)
export SYSTEM_PATH="$(pwd)/../"
export APPS_PATH="$(pwd)/../../"

DOCKER_COMPOSE_FILE="../var/docker/docker-compose.yml"

if [ -f $DOCKER_COMPOSE_FILE ]; then
    echo 'Deleting old docker-compose file'
    rm $DOCKER_COMPOSE_FILE
fi

echo '\nDeleting old docker containers'
docker rm -f $(docker ps -aq --filter "name=system") || true

echo '\nDeleting old router confs'
rm -rf ../var/confs/*

echo '\nDeploying docker confs for apps'
# system must be called last
for APP in assmat system
do
    sh -c "cd ../../$APP/app/.deploy/scripts/ && sh docker.sh"
done

docker-compose -f $DOCKER_COMPOSE_FILE up -d

echo '\nSystem is up !'
