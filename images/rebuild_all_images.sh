#!/bin/sh

for i in supervisor nginx router php-apache; do
  cd $i
  make
  cd -
done
